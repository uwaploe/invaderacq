# inVADER Data Acquisition Apps

This repository contains a set of CLI applications for inVADER data acquistion. To install, download the latest tarball from the "Downloads" page, unpack, and install all of the executables to the `~sysop/bin/`directory.

## Installation

Download the latest compressed TAR file from the "Downloads" page, unpack it, and use the [stow](https://www.gnu.org/software/stow/manual/stow.html) command to link the executable files into the `~sysop/bin/` directory:

``` sh
cd ~/stow
curl -sS -L https://bitbucket.org/uwaploe/invaderacq/downloads/invaderacq_0.7.0_linux_x86_64.tar.gz | tar -xzf -
stow --target ~/bin/ invaderacq_0.7.0_linux_x86_64
```

### Upgrading

When upgrading to a new release, you must first remove all links to the current version by using the `-D` command-line option as shown below:

``` sh
cd ~/stow
curl -sS -L https://bitbucket.org/uwaploe/invaderacq/downloads/invaderacq_0.8.0_linux_x86_64.tar.gz | tar -xzf -
stow --target ~/bin/ -D invaderacq_0.7.0_linux_x86_64
stow --target ~/bin/ invaderacq_0.8.0_linux_x86_64
```

## Usage

Each application provides a usage message if you run it with the *--help* command-line option.
