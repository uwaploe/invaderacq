// Interactively adjust the inVADER Beam Expander
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"
	"time"

	"bitbucket.org/uwaploe/beamexp"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/tarm/serial"
)

const Usage = `Usage: bectl [options]

Interactively control a Sill Optics Beam Expander.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	expDev  string = "/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A907FQ0N-if00-port0"
	expBaud int    = 9600
)

type model struct {
	textInput textinput.Model
	err       error
	dev       *beamexp.Device
}

func initialModel(dev *beamexp.Device) model {
	ti := textinput.NewModel()
	ti.Placeholder = "Divergence value"
	ti.Focus()
	ti.CharLimit = 16
	ti.Width = 20

	offset, _ := dev.Offset()
	ti.SetValue(fmt.Sprintf("%d", offset))
	ti.CursorEnd()

	return model{
		textInput: ti,
		dev:       dev,
	}
}

func (m model) Init() tea.Cmd {
	return textinput.Blink
}

type errMsg error

func (m *model) decCounter(val int) {
	var offset int
	offset, m.err = m.dev.Offset()
	if offset >= val {
		offset -= val
		m.dev.SetOffset(offset)
	}
	m.textInput.SetValue(fmt.Sprintf("%d", offset))
}

func (m *model) incCounter(val int) {
	var offset int
	offset, m.err = m.dev.Offset()
	if offset <= (3200 - val) {
		offset += val
		m.dev.SetOffset(offset)
	}
	m.textInput.SetValue(fmt.Sprintf("%d", offset))
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.Type {
		case tea.KeyCtrlC, tea.KeyEsc:
			return m, tea.Quit
		case tea.KeyUp:
			m.incCounter(1)
		case tea.KeyPgUp:
			m.incCounter(10)
		case tea.KeyDown:
			m.decCounter(1)
		case tea.KeyPgDown:
			m.decCounter(10)
		case tea.KeyEnter:
			var offset int
			if x, err := strconv.ParseUint(m.textInput.Value(), 0, 16); err == nil {
				if x <= 3200 {
					m.dev.SetOffset(int(x))
					offset, m.err = m.dev.Offset()
				}
			}
			m.textInput.SetValue(fmt.Sprintf("%d", offset))
		case tea.KeyRunes:
			switch msg.Runes[0] {
			case '+':
				m.incCounter(1)
				return m, cmd
			case '-':
				m.decCounter(1)
				return m, cmd
			}
		}
	case errMsg:
		m.err = msg
		return m, nil
	}

	m.textInput, cmd = m.textInput.Update(msg)
	return m, cmd
}

func (m model) View() string {
	return fmt.Sprintf(
		"Beam Expander control\n\n%s\n\n%s",
		m.textInput.View(),
		"(esc to quit)",
	) + "\n"
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&expDev, "dev", expDev, "beam expander serial device")
	flag.IntVar(&expBaud, "baud", expBaud, "beam expander baud rate")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	_ = parseCmdLine()

	port, err := serial.OpenPort(&serial.Config{
		Name:        expDev,
		Baud:        expBaud,
		ReadTimeout: time.Second * 5})
	if err != nil {
		log.Fatalf("Cannot open serial device %q: %v", expDev, err)
	}
	prog := tea.NewProgram(initialModel(beamexp.New(port)))

	if err := prog.Start(); err != nil {
		log.Fatal(err)
	}
}
