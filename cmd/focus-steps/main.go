//
package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"runtime"
	"strconv"

	"bitbucket.org/uwaploe/invaderacq/internal/app"
	"bitbucket.org/uwaploe/invaderacq/internal/imager"
	"golang.org/x/exp/slog"
)

const Usage = `Usage: focus-steps [options] cfgfile count

Read the inVADER data acquisition configuration file and image acquisition
count and output the focus motor position for each image.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func initLogger() *slog.Logger {
	opts := slog.HandlerOptions{}
	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		opts.ReplaceAttr = func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				return slog.Attr{}
			}
			return a
		}
	}

	return slog.New(opts.NewTextHandler(os.Stderr))
}

func showSteps(w io.Writer, fs *imager.FocusStepper, n int) {
	for n > 0 {
		step, _ := fs.Step()
		fmt.Fprintf(w, "%d\n", step)
		n--
	}
}

func main() {
	args := parseCmdLine()
	if len(args) < 2 {
		flag.Usage()
		os.Exit(2)
	}

	slog.SetDefault(initLogger())

	cfg, err := app.LoadConfig(args[0])
	if err != nil {
		slog.Error("load configuration", err)
		os.Exit(1)
	}
	count, err := strconv.Atoi(args[1])
	if err != nil {
		slog.Error("invalid image count", err)
		os.Exit(1)
	}

	switch {
	case cfg.Focus.Change != "":
		fs, err := imager.NewStepper(cfg.Focus)
		if err != nil {
			slog.Error("", err)
			os.Exit(1)
		}
		showSteps(os.Stdout, fs, count)
	case cfg.Focus.Start > 0:
		cfg.Focus.Change = "+ 0"
		cfg.Focus.EndState = fmt.Sprintf("== %d", cfg.Focus.Start)
		fs, err := imager.NewStepper(cfg.Focus)
		if err != nil {
			slog.Error("", err)
			os.Exit(1)
		}
		showSteps(os.Stdout, fs, count)
	default:
		slog.Info("No focus steps")
		os.Exit(0)
	}
}
