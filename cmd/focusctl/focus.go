package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
)

var ErrFmt = errors.New("Bad parameter format")

// Convert focus distance in meters to motor counts using the
// equation f(x) = A*(x-B)^k where x is the focus distance.
type distanceEqn struct {
	A, B, k float64
}

func (eq distanceEqn) Counts(meters float64) int32 {
	return int32(math.Pow(meters-eq.B, eq.k) * eq.A)
}

func (eq distanceEqn) Meters(counts int32) float64 {
	x := float64(counts) / eq.A
	return math.Pow(x, 1./eq.k) + eq.B
}

func (eq *distanceEqn) Set(val string) (err error) {
	fields := strings.Split(val, ",")
	if len(fields) != 3 {
		err = fmt.Errorf("%w: %q", ErrFmt, val)
		return
	}
	eq.A, err = strconv.ParseFloat(fields[0], 64)
	if err != nil {
		return
	}
	if fields[1] == "" {
		eq.B = 0
	} else {
		eq.B, err = strconv.ParseFloat(fields[1], 64)
		if err != nil {
			return
		}
	}
	eq.k, err = strconv.ParseFloat(fields[2], 64)
	if err != nil {
		return
	}

	return nil
}

func (eq distanceEqn) String() string {
	return fmt.Sprintf("%.3f,%.3f,%.3f", eq.A, eq.B, eq.k)
}

func (eq *distanceEqn) UnmarshalText(b []byte) error {
	return eq.Set(string(b))
}

func (eq *distanceEqn) UnmarshalJSON(b []byte) error {
	m := make(map[string]float64)
	if err := json.Unmarshal(b, &m); err != nil {
		return err
	}
	eq.A = m["A"]
	eq.B = m["B"]
	eq.k = m["k"]

	return nil
}
