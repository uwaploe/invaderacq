// Focusctl controls the inVADER CCD focus motor
package main

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"time"

	"bitbucket.org/uwaploe/invaderacq/internal/focus"
	"bitbucket.org/uwaploe/pducom"
	"github.com/urfave/cli"
)

var Version = "dev"
var BuildDate = "unknown"

func main() {
	app := cli.NewApp()
	app.Name = "focusctl"
	app.Usage = "Control the inVADER focus motor via the PDU"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "addr",
			Usage:  "Redis server `HOST:PORT`",
			Value:  "localhost:6379",
			EnvVar: "INV_REDIS_HOST",
		},
		cli.DurationFlag{
			Name:  "timeout, t",
			Usage: "timeout `DURATION`",
			Value: time.Second * 10,
		},
		cli.UintFlag{
			Name:  "dac",
			Usage: "motor output DAC value",
			Value: 0,
		},
		cli.GenericFlag{
			Name:   "params",
			Usage:  "focus position eqn parameters, `A,B,k`",
			Value:  &distanceEqn{A: 981558, k: -1.35},
			EnvVar: "FOCUS_EQN",
		},
		cli.VersionFlag,
	}

	var motor *focus.Motor
	app.Before = func(c *cli.Context) error {
		var err error
		motor, err = focus.New(c.String("addr"))
		if err != nil {
			return err
		}
		if dac := c.Uint("dac"); dac > 0 {
			motor.SetDAC(uint16(dac))
		}
		return nil
	}

	app.After = func(c *cli.Context) error {
		motor.Close()
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:  "status",
			Usage: "display focus motor status",
			Action: func(c *cli.Context) error {
				ctx, cancel := context.WithTimeout(context.Background(),
					c.GlobalDuration("timeout"))
				defer cancel()
				eqn := c.GlobalGeneric("params").(*distanceEqn)
				status, err := motor.WaitForStatus(ctx)
				if err != nil {
					return cli.NewExitError("Cannot get status", 1)
				}
				fmt.Printf("state=%s braking=%t counter=%d focus=%.2f\n", status.State,
					status.Braking, status.Counts, eqn.Meters(status.Counts))
				return nil
			},
		},
		{
			Name:  "home",
			Usage: "rotate motor to lower limit and zero the counter",
			Action: func(c *cli.Context) error {
				ctx, cancel := context.WithTimeout(context.Background(),
					c.GlobalDuration("timeout"))
				defer cancel()
				eqn := c.GlobalGeneric("params").(*distanceEqn)
				if _, err := motor.WaitForStatus(ctx); err != nil {
					return cli.NewExitError("Cannot get status", 2)
				}
				if err := motor.Home(ctx); err != nil {
					return cli.NewExitError(fmt.Errorf("HOME failed: %v", err), 3)
				}
				status := motor.Status()
				fmt.Printf("state=%s braking=%t counter=%d focus=%.2f\n", status.State,
					status.Braking, status.Counts, eqn.Meters(status.Counts))
				return nil
			},
		},
		{
			Name:      "seek",
			Usage:     "rotate motor to a position in counts or focus meters",
			ArgsUsage: "POSITION",
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name:   "meters",
					Usage:  "specify focus position in meters",
					EnvVar: "FOCUS_METERS",
				},
			},
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				eqn := c.GlobalGeneric("params").(*distanceEqn)
				pos, err := strconv.ParseFloat(c.Args().Get(0), 64)
				if err != nil {
					return cli.NewExitError("bad COUNTS value", 1)
				}

				var counts int32
				if c.Bool("meters") {
					counts = eqn.Counts(pos)
				} else {
					counts = int32(pos)
				}

				if err := motor.Seek(counts); err != nil {
					return err
				}

				ctx, cancel := context.WithTimeout(context.Background(),
					c.GlobalDuration("timeout"))
				defer cancel()
				var (
					status pducom.FocusStatus
				)

				if status, err = motor.WaitForState(ctx, "SEEK", "IDLE"); err != nil {
					fmt.Printf("state=%s braking=%t counter=%d focus=%.2f\n", status.State,
						status.Braking, status.Counts, eqn.Meters(status.Counts))
					return cli.NewExitError("SEEK/IDLE state not seen", 2)
				}
				status, err = motor.WaitForState(ctx, "IDLE", "LIMIT")
				if err != nil {
					fmt.Printf("state=%s braking=%t counter=%d focus=%.2f\n", status.State,
						status.Braking, status.Counts, eqn.Meters(status.Counts))
					return cli.NewExitError(fmt.Errorf("SEEK failed: %v", err), 3)
				}
				fmt.Printf("state=%s braking=%t counter=%d focus=%.2f\n", status.State,
					status.Braking, status.Counts, eqn.Meters(status.Counts))
				return nil
			},
		},
	}

	app.Run(os.Args)
}
