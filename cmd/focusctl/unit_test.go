package main

import (
	"math"
	"testing"
)

func relerr(x, y float64) float64 {
	return math.Abs(y-x) / y
}

func TestEqn(t *testing.T) {
	tests := []struct {
		params string
		in     float64
		out    int32
	}{
		{params: "981558,,-1.35", in: 2.337, out: 312051},
		{params: "981558,2,-1.35", in: 2.337 + 2, out: 312051},
		{params: "981558,-1,-1.35", in: 2.337 - 1, out: 312051},
	}

	for _, e := range tests {
		eqn := &distanceEqn{}
		eqn.Set(e.params)
		if out := eqn.Counts(e.in); out != e.out {
			t.Errorf("Bad value; expected %d, got %d", e.out, out)
		}
		m := eqn.Meters(e.out)
		if relerr(m, e.in) > 0.001 {
			t.Errorf("Bad value; expected %v, got %v", e.in, m)
		}
	}
}
