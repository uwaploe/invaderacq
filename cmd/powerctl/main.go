// Powerctl switches power to the inVADER subsystems
package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/uwaploe/invaderacq/internal/power"
	"bitbucket.org/uwaploe/pducom"
	"github.com/urfave/cli"
)

const Usage = `Usage: powerctl [options] on|off|check name [name ...]

Switch the power on or off to the listed subsystems.
`

var Version = "dev"
var BuildDate = "unknown"

func showStatus(status pducom.Power, subsys ...string) {
	state := map[bool]string{false: "off", true: "on"}
	if len(subsys) == 0 {
		for k, v := range status {
			fmt.Printf("%s: %s\n", k, state[v])
		}
	} else {
		for _, sys := range subsys {
			fmt.Printf("%s: %s\n", sys, state[status[sys]])
		}
	}
}

func main() {
	app := cli.NewApp()
	app.Name = "powerctl"
	app.Usage = "Switch power to inVADER subsystems via the PDU"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "addr",
			Usage:  "Redis server `HOST:PORT`",
			Value:  "localhost:6379",
			EnvVar: "INV_REDIS_HOST",
		},
		cli.DurationFlag{
			Name:  "wait, w",
			Usage: "time to wait for confirmation",
			Value: time.Second * 6,
		},
		cli.VersionFlag,
	}

	var mon *power.Monitor
	app.Before = func(c *cli.Context) error {
		var err error
		mon, err = power.New(c.String("addr"))
		if err != nil {
			return err
		}
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:      "on",
			Usage:     "power-on one or more subsystems",
			ArgsUsage: "SUBSYS...",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				if err := mon.On(c.Args()...); err != nil {
					return cli.NewExitError(fmt.Errorf("Operation failed: %v", err), 2)
				}
				if wait := c.GlobalDuration("wait"); wait > 0 {
					ctx, cancel := context.WithTimeout(context.Background(), wait)
					defer cancel()
					if err := mon.WaitForOn(ctx, c.Args()...); err != nil {
						return cli.NewExitError(fmt.Errorf("wait failed: %v", err), 3)
					}
				}
				return nil
			},
		},
		{
			Name:      "off",
			Usage:     "power-off one or more subsystems",
			ArgsUsage: "SUBSYS...",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				if err := mon.Off(c.Args()...); err != nil {
					return cli.NewExitError(fmt.Errorf("Operation failed: %v", err), 2)
				}
				if wait := c.GlobalDuration("wait"); wait > 0 {
					ctx, cancel := context.WithTimeout(context.Background(), wait)
					defer cancel()
					if err := mon.WaitForOff(ctx, c.Args()...); err != nil {
						return cli.NewExitError(fmt.Errorf("wait failed: %v", err), 3)
					}
				}
				return nil
			},
		},
		{
			Name:      "check",
			Usage:     "check the state of one or more subsystems (default: all)",
			ArgsUsage: "[SUBSYS...]",
			Action: func(c *cli.Context) error {
				ctx, cancel := context.WithTimeout(context.Background(), c.GlobalDuration("wait"))
				defer cancel()
				status, err := mon.WaitForStatus(ctx)
				if err != nil {
					return cli.NewExitError(fmt.Errorf("wait failed: %v", err), 3)
				}
				showStatus(status, c.Args()...)
				return nil
			},
		},
	}

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	app.Run(os.Args)
}
