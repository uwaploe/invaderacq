module bitbucket.org/uwaploe/invaderacq

go 1.20

require (
	bitbucket.org/uwaploe/beamexp v0.4.0
	bitbucket.org/uwaploe/iccdctl v0.9.2
	bitbucket.org/uwaploe/imagesvc v0.8.0
	bitbucket.org/uwaploe/pducom v0.9.6
	bitbucket.org/uwaploe/tbctl v0.6.1
	bitbucket.org/uwaploe/viron v0.9.9
	github.com/BurntSushi/toml v0.3.1
	github.com/alicebob/miniredis/v2 v2.14.3
	github.com/briandowns/spinner v1.9.0
	github.com/charmbracelet/bubbles v0.7.6
	github.com/charmbracelet/bubbletea v0.13.1
	github.com/felixge/httpsnoop v1.0.1
	github.com/gomodule/redigo v1.8.3
	github.com/gorilla/mux v1.8.0
	github.com/maja42/goval v1.3.1
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	github.com/urfave/cli v1.22.5
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29
	google.golang.org/grpc v1.27.0
)

require (
	bitbucket.org/mfkenney/go-nmea v1.2.1 // indirect
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/atotto/clipboard v0.1.2 // indirect
	github.com/containerd/console v1.0.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/golang/protobuf v1.4.1 // indirect
	github.com/lucasb-eyer/go-colorful v1.0.3 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.10 // indirect
	github.com/muesli/reflow v0.2.1-0.20210115123740-9e1d0d53df68 // indirect
	github.com/muesli/termenv v0.7.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/net v0.0.0-20191009170851-d66e71096ffb // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
