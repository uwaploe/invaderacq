package app

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaploe/invaderacq/internal/imager"
	"bitbucket.org/uwaploe/invaderacq/internal/laser"
	"bitbucket.org/uwaploe/tbctl/pkg/tb"
	"bitbucket.org/uwaploe/viron"
	"github.com/briandowns/spinner"
)

const Usage = `Usage: invaderacq [options] paramfile [count]

Run an inVADER data acquisition sequence and collect COUNT images.
`

type listVal []string

func (l *listVal) String() string {
	return fmt.Sprint(*l)
}

func (l *listVal) Set(value string) error {
	for _, tag := range strings.Split(value, ",") {
		*l = append(*l, tag)
	}
	return nil
}

type appEnv struct {
	fl        *flag.FlagSet
	vers      string
	paramFile string
	imgCount  int
	imOpts    []imager.Option
	laserOpts []laser.Option
	tbAddr    string
	outDir    string
	mdFile    string
	standBy   bool
	statFile  string
	tags      listVal
	bgImage   bool
	warnOk    bool
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("invader", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	app.imOpts = make([]imager.Option, 0)
	app.laserOpts = make([]laser.Option, 0)

	showVers := fl.Bool("version", false, "Show program version information and exit")
	imSvc := fl.String("server", "", "host:port for image acquisition server")
	intensDev := fl.String("intens-dev", "", "image intensifier serial device")
	laserAddr := fl.String("laser-addr",
		lookupEnvOrString("INV_VIRON_ADDR", ""), "Viron laser host:port")
	laserPword := fl.String("laser-pword",
		lookupEnvOrString("INV_VIRON_PWORD", ""), "Viron laser password")
	fl.StringVar(&app.tbAddr, "tb-sock", "/run/tb.sock", "Timing Board server socket")
	fl.StringVar(&app.outDir, "image-dir",
		lookupEnvOrString("INV_IMAGE_DIR", "."), "image storage directory")
	fl.StringVar(&app.mdFile, "meta",
		lookupEnvOrString("INV_IMAGE_META", ""), "metadata file")
	fl.BoolVar(&app.standBy, "standby", false, "keep laser in STANDBY mode when done")
	fl.BoolVar(&app.bgImage, "bg", false, "acquire a background image without laser firing")
	fl.StringVar(&app.statFile, "laser-stat", "", "file to log laser warmup status")
	fl.BoolVar(&app.warnOk, "warn-ok", false, "ignore laser warnings during warmup")
	fl.Var(&app.tags, "tag", "comma separated list of tags to include with the metadata")

	app.fl = fl
	if err := fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) == 0 {
		fmt.Fprintf(os.Stderr, "Missing parameter file")
		fl.Usage()
		return flag.ErrHelp
	}
	app.paramFile = req[0]

	if len(req) > 1 {
		app.imgCount, _ = strconv.Atoi(req[1])
	}

	if app.imgCount == 0 {
		app.imgCount = 1
	}

	if *imSvc != "" {
		app.imOpts = append(app.imOpts, imager.ImageAcqServer(*imSvc))
	}

	if *intensDev != "" {
		app.imOpts = append(app.imOpts, imager.IntensifierDevice(*intensDev))
	}

	if *laserAddr != "" {
		app.laserOpts = append(app.laserOpts, laser.VironServer(*laserAddr))
	}

	if *laserPword != "" {
		app.laserOpts = append(app.laserOpts, laser.VironPassword(*laserPword))
	}

	return nil
}

type metaData struct {
	Image    string    `json:"image"`
	Tags     []string  `json:"tags,omitempty"`
	Settings SysConfig `json:"settings"`
}

func appendMetadata(filename string, cfg SysConfig, res []imager.AcqResult, tags listVal) error {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	enc := json.NewEncoder(f)
	for _, obj := range res {
		cfg.Focus.Setting = obj.Focus
		err = enc.Encode(metaData{Image: filepath.Base(obj.Name), Tags: tags, Settings: cfg})
		if err != nil {
			return err
		}
	}

	return nil
}

type infoRec struct {
	T    time.Time  `json:"time"`
	Info viron.Info `json:"info"`
}

// Generate a filename for the background image
func bgFilename(t time.Time, _ int) string {
	s := t.Format("20060102_150405")
	return fmt.Sprintf("bg_%s_%03d.tiff", s, t.Nanosecond()/1000000)
}

func (app *appEnv) run(ctx0 context.Context) (err error) {
	// Load the parameter file
	cfg, err := LoadConfig(app.paramFile)
	if err != nil {
		return err
	}

	// Validate the timing board values
	if err = cfg.Timing.Validate(tb.FpgaClock); err != nil {
		return err
	}

	// Load the timing board parameters
	FpgaLoad.Data, _ = json.Marshal(cfg.Timing)
	err = sendMessages(app.tbAddr, FpgaDisable, FpgaLoad)
	if err != nil {
		return fmt.Errorf("Cannot send Timing Board message: %w", err)
	}

	im, err := imager.New(app.imOpts...)
	if err != nil {
		return err
	}

	l, err := laser.New(app.laserOpts...)
	if err != nil {
		return err
	}

	log.Println("Configuring ICCD")
	err = im.Configure(imager.Config{Iccd: cfg.Iccd, Focus: cfg.Focus})
	if err != nil {
		return err
	}
	defer im.Shutdown()

	log.Println("Configuring laser")
	err = l.Configure(laser.Config{Exp: cfg.Exp, Viron: cfg.Viron})
	if err != nil {
		return err
	}

	// Acquire an image with the laser disabled (in STANDBY mode)
	if app.bgImage {
		log.Println("Acquiring background image")
		log.Println("Enabling trigger")
		err = sendMessages(app.tbAddr, FpgaEnable)
		if err != nil {
			return err
		}
		defer sendMessages(app.tbAddr, FpgaDisable)
		result, err := im.Acquire(ctx0,
			imager.AcqOptions{Count: 1, Dir: app.outDir, Filename: bgFilename})
		if err != nil {
			log.Printf("Acquisition failed: %v", err)
		} else {
			if app.mdFile != "" {
				appendMetadata(app.mdFile, cfg, result, append(app.tags, "background"))
			}
		}
	}

	var info_ch chan viron.Info
	done := make(chan struct{}, 1)

	// Log the laser warm-up status to a file
	if app.statFile != "" {
		info_ch = make(chan viron.Info, 1)
		f, err := os.OpenFile(app.statFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		// Goroutine to read from info_ch and write the file
		go func() {
			defer f.Close()
			enc := json.NewEncoder(f)
			for {
				select {
				case <-done:
					return
				case val := <-info_ch:
					enc.Encode(infoRec{T: time.Now(), Info: val})
				}
			}
		}()
	}

	ctx, cancel := context.WithTimeout(ctx0, time.Duration(cfg.Viron.Warmup))

	// Eye candy while the laser is warming up
	s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
	s.Prefix = "Waiting for laser warm-up ... "
	s.FinalMSG = "\n"

	// Run the Prepare method in a goroutine while the spinner
	// is shown
	laser_err := make(chan error, 1)
	go func() { laser_err <- l.Prepare(ctx, info_ch, app.warnOk) }()

	s.Start()
	err = <-laser_err
	cancel()
	// Signal the status-file writer to stop
	close(done)
	s.Stop()

	if err != nil {
		return err
	}

	if app.standBy {
		defer func() {
			err = errors.Join(err, l.Shutdown())
		}()
	} else {
		defer func() {
			err = errors.Join(err, l.Close())
		}()
	}

	// If we acquired a background image, the FPGA output
	// is already enabled...
	if !app.bgImage {
		log.Println("Enabling trigger")
		err = sendMessages(app.tbAddr, FpgaEnable)
		if err != nil {
			return err
		}
		defer sendMessages(app.tbAddr, FpgaDisable)
	}

	log.Println("Acquiring images")
	names, err := im.Acquire(ctx0,
		imager.AcqOptions{Count: app.imgCount, Dir: app.outDir})
	if app.mdFile != "" {
		appendMetadata(app.mdFile, cfg, names, app.tags)
	}

	log.Println("Done")
	return err
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	app.vers = vers
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}
	if err = app.run(ctx); err != nil {
		fmt.Fprintf(os.Stderr, "Runtime error: %v\n", err)
		return 1
	}
	return 0
}
