package app

import (
	"fmt"
	"io/ioutil"

	"bitbucket.org/uwaploe/invaderacq/internal/imager"
	"bitbucket.org/uwaploe/invaderacq/internal/laser"
	"bitbucket.org/uwaploe/tbctl/pkg/tb"
	"github.com/BurntSushi/toml"
)

type SysConfig struct {
	Timing tb.Timing          `toml:"timing"`
	Viron  laser.VironConfig  `toml:"viron"`
	Exp    laser.ExpConfig    `toml:"exp"`
	Iccd   imager.IccdConfig  `toml:"iccd"`
	Focus  imager.FocusConfig `toml:"focus"`
}

func LoadConfig(filename string) (SysConfig, error) {
	var cfg SysConfig

	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return cfg, fmt.Errorf("read parameter file: %w", err)
	}

	_, err = toml.Decode(string(contents), &cfg)

	return cfg, err
}
