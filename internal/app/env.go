package app

import (
	"log"
	"os"
	"strconv"
	"time"
)

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func lookupEnvOrInt(key string, defaultVal int) int {
	if val, ok := os.LookupEnv(key); ok {
		v, err := strconv.Atoi(val)
		if err != nil {
			log.Printf("LookupEnvOrInt[%s]: %v", key, err)
			return defaultVal
		}
		return v
	}
	return defaultVal
}

func lookupEnvOrFloat(key string, defaultVal float64) float64 {
	if val, ok := os.LookupEnv(key); ok {
		v, err := strconv.ParseFloat(val, 64)
		if err != nil {
			log.Printf("LookupEnvOrFloat[%s]: %v", key, err)
			return defaultVal
		}
		return v
	}
	return defaultVal
}

func lookupEnvOrDuration(key string, defaultVal time.Duration) time.Duration {
	if val, ok := os.LookupEnv(key); ok {
		v, err := time.ParseDuration(val)
		if err != nil {
			log.Printf("LookupEnvOrDuration[%s]: %v", key, err)
			return defaultVal
		}
		return v
	}
	return defaultVal
}
