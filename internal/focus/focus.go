// Package focus controls the inVADER focus motor
package focus

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/uwaploe/invaderacq/internal/pducmd"
	"bitbucket.org/uwaploe/pducom"
	"github.com/gomodule/redigo/redis"
)

const fstatChannel = "data.pdu.FSTAT"

type Motor struct {
	cmdr   *pducmd.Pducmd
	status pducom.FocusStatus
	psc    redis.PubSubConn
	cond   *sync.Cond
	done   chan struct{}
}

// New returns a new Motor. Addr is the host:port of the Redis server which
// will provide the control interface.
func New(addr string) (*Motor, error) {
	conn, err := redis.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}
	psc := redis.PubSubConn{Conn: conn}
	err = psc.Subscribe(fstatChannel)
	if err != nil {
		return nil, err
	}
	m := &Motor{
		cmdr: pducmd.New(addr),
		done: make(chan struct{}, 1),
		cond: sync.NewCond(&sync.Mutex{}),
		psc:  psc,
	}
	go m.listen(psc)

	// Send a periodic PING to ensure that the listen goroutine doesn't
	// get "stuck".
	ticker := time.NewTicker(time.Second * 5)
	go func() {
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				if err := psc.Ping(""); err != nil {
					psc.Unsubscribe()
					// The unsubscribe call will cause m.listen to
					// return and close the m.done channel which will
					// in-turn cause this goroutine to return.
				}
			case <-m.done:
				return
			}
		}
	}()

	return m, nil
}

func (m *Motor) Close() {
	if m != nil && m.psc.Conn != nil {
		m.psc.Unsubscribe()
	}
}

func anyEqual(a string, b ...string) bool {
	for _, v := range b {
		if a == v {
			return true
		}
	}
	return false
}

// WaitForState returns the Motor status when the Motor reaches any of the
// named states or when the Context is canceled.
func (m *Motor) WaitForState(ctx context.Context, states ...string) (pducom.FocusStatus, error) {
	m.cond.L.Lock()
	defer m.cond.L.Unlock()
	for !anyEqual(m.status.State, states...) {
		select {
		case <-ctx.Done():
			return m.status, ctx.Err()
		case <-m.done:
			return m.status, errors.New("Listener stopped")
		default:
			m.cond.Wait()
		}
	}
	return m.status, nil
}

// WaitFor Status waits for the initial status message from the PDU
func (m *Motor) WaitForStatus(ctx context.Context) (pducom.FocusStatus, error) {
	m.cond.L.Lock()
	defer m.cond.L.Unlock()
	for m.status.State == "" {
		select {
		case <-ctx.Done():
			return m.status, ctx.Err()
		case <-m.done:
			return m.status, errors.New("Listener stopped")
		default:
			m.cond.Wait()
		}
	}
	return m.status, nil
}

// Home rotates the Motor to its "home" position (minimum focus) and zeros
// the counter.
func (m *Motor) Home(ctx context.Context) error {
	for {
		m.cmdr.Send("FOCUS,JOG,R,1000")
		if _, err := m.WaitForState(ctx, "JOG", "LIMIT"); err != nil {
			m.cmdr.Send("FOCUS,STOP")
			return fmt.Errorf("waiting for JOG/LIMIT state: %w", err)
		}
		status, err := m.WaitForState(ctx, "LIMIT", "IDLE")
		if err != nil {
			m.cmdr.Send("FOCUS,STOP")
			return fmt.Errorf("waiting for LIMIT/IDLE state: %w", err)
		}
		if status.State == "LIMIT" {
			m.cmdr.Send("FOCUS,ZERO")
			break
		}
	}

	return nil
}

// Seek starts rotating the Motor to new position pos.
func (m *Motor) Seek(pos int32) error {
	return m.cmdr.Send(fmt.Sprintf("FOCUS,SEEK,%d", pos))
}

// Status returns the most recent Motor status.
func (m *Motor) Status() pducom.FocusStatus {
	return m.status
}

// SetDAC sets the output level of the Motor DAC
func (m *Motor) SetDAC(level uint16) error {
	return m.cmdr.Send(fmt.Sprintf("FDAC,%04X", level))
}

// Goto rotates the Motor to an absolute position
func (m *Motor) Goto(ctx context.Context, pos int32) (pducom.FocusStatus, error) {
	var status pducom.FocusStatus
	err := m.Seek(pos)
	if err != nil {
		return status, err
	}
	_, err = m.WaitForState(ctx, "SEEK")
	if err != nil {
		return status, fmt.Errorf("wait for SEEK: %w", err)
	}

	return m.WaitForState(ctx, "IDLE")
}

type rawMessage struct {
	T    time.Time          `json:"time"`
	Src  string             `json:"source"`
	Data pducom.FocusStatus `json:"data"`
}

// Monitor the Redis pub-sub channel data.pdu.FSTAT and update the
// Motor.status field.
func (m *Motor) listen(psc redis.PubSubConn) {
	defer close(m.done)
	for {
		switch msg := psc.Receive().(type) {
		case error:
			return
		case redis.Subscription:
			if msg.Count == 0 {
				return
			}
		case redis.Pong:
			m.cond.Broadcast()
		case redis.Message:
			rec := rawMessage{}
			if err := json.Unmarshal(msg.Data, &rec); err != nil {
				continue
			}
			m.cond.L.Lock()
			m.status = rec.Data
			m.cond.Broadcast()
			m.cond.L.Unlock()
		}
	}
}
