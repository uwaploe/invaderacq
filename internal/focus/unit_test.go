package focus

import (
	"context"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
)

func TestStatusWait(t *testing.T) {
	s, err := miniredis.Run()
	if err != nil {
		t.Fatal(err)
	}
	defer s.Close()

	motor, err := New(s.Addr())
	if err != nil {
		t.Fatal(err)
	}
	defer motor.Close()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	go func() {
		time.Sleep(time.Second * 2)
		s.Publish(fstatChannel,
			`{"time":"2021-03-18T00:00:00.877147017Z","source":"FSTAT","data":{"state":"IDLE","counts":962462}}`)
	}()

	stat, err := motor.WaitForStatus(ctx)
	if err != nil {
		t.Fatal(err)
	}

	if stat.Counts != 962462 {
		t.Errorf("FSTAT decode error: %#v", stat)
	}
}

func TestAnyEqual(t *testing.T) {
	table := []struct {
		a      string
		b      []string
		result bool
	}{
		{
			a:      "jog",
			b:      []string{"idle", "limit"},
			result: false,
		},
		{
			a:      "jog",
			b:      []string{"jog"},
			result: true,
		},
		{
			a:      "",
			b:      []string{"foo", "bar", "baz"},
			result: false,
		},
		{
			a:      "baz",
			b:      []string{"foo", "bar", "baz"},
			result: true,
		},
	}

	for _, e := range table {
		result := anyEqual(e.a, e.b...)
		if result != e.result {
			t.Errorf("Bad result for anyEqual(%s, %s)", e.a, e.b)
		}
	}

}
