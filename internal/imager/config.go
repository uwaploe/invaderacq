package imager

import (
	iccd "bitbucket.org/uwaploe/iccdctl"
	"bitbucket.org/uwaploe/tbctl/pkg/tb"
)

type IccdConfig struct {
	Gate     iccd.Gate       `json:"gate" toml:"gate"`
	Sync     iccd.SyncSource `json:"sync" toml:"sync"`
	Igain    float32         `json:"igain" toml:"igain"`
	Gain     float32         `json:"gain" toml:"gain"`
	Exposure tb.Duration     `json:"exposure" toml:"exposure"`
}

type FocusConfig struct {
	// Initial focus setting
	Start int `json:"-" toml:"start"`
	// Amount to change the focus after each image acquisition.
	//
	//    OP VALUE
	//
	// Where OP is * or / or + or - and value is a floating
	// point number.
	Change string `json:"-" toml:"change"`
	// The state which causes the focus change to stop.
	//
	//    OP VALUE
	//
	// Where OP is >, >=, <, or <= and value is a floating
	// point number.
	EndState string `json:"-" toml:"end_state"`
	// This field is used in the output metadata to record the
	// focus setting for each image.
	Setting int `json:"setting,omitempty" toml:"setting"`
}

type Config struct {
	Iccd  IccdConfig  `json:"iccd" toml:"iccd"`
	Focus FocusConfig `json:"focus" toml:"focus"`
}
