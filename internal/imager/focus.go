//
package imager

import (
	"fmt"

	"github.com/maja42/goval"
)

type FocusStepper struct {
	pos      int
	ev       *goval.Evaluator
	loopExpr string
	endExpr  string
}

func NewStepper(cfg FocusConfig) (*FocusStepper, error) {
	s := &FocusStepper{
		pos: cfg.Start,
		ev:  goval.NewEvaluator(),
	}

	s.loopExpr = "pos " + cfg.Change
	result, err := s.ev.Evaluate(s.loopExpr, map[string]interface{}{"pos": float64(s.pos)}, nil)
	if err != nil {
		return nil, fmt.Errorf("Invalid expression; %q: %w", s.loopExpr, err)
	}
	if _, ok := result.(float64); !ok {
		return nil, fmt.Errorf("Invalid expression; %q", s.loopExpr)
	}

	s.endExpr = "pos " + cfg.EndState
	result, err = s.ev.Evaluate(s.endExpr, map[string]interface{}{"pos": float64(s.pos)}, nil)
	if err != nil {
		return nil, fmt.Errorf("Invalid expression; %q: %w", s.endExpr, err)
	}
	if _, ok := result.(bool); !ok {
		return nil, fmt.Errorf("Invalid expression; %q", s.endExpr)
	}

	return s, nil
}

func (s *FocusStepper) Step() (int, bool) {
	var done bool

	pos := s.pos
	result, _ := s.ev.Evaluate(s.endExpr, map[string]interface{}{"pos": float64(s.pos)}, nil)
	if done = result.(bool); done == false {
		result, _ = s.ev.Evaluate(s.loopExpr, map[string]interface{}{"pos": float64(s.pos)}, nil)
		s.pos = int(result.(float64))
	}

	return pos, done
}
