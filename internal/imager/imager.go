// Package imager manages the inVADER ICCD and focus motor.
package imager

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	iccd "bitbucket.org/uwaploe/iccdctl"
	"bitbucket.org/uwaploe/imagesvc"
	"bitbucket.org/uwaploe/invaderacq/internal/focus"
	"bitbucket.org/uwaploe/invaderacq/internal/port"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func grpcClient(address string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		return nil, err
	}

	return conn, err
}

type Imager struct {
	intensifier *iccd.Intensifier
	iClient     *imagesvc.Client
	mtr         *focus.Motor
	fstep       *FocusStepper
	closers     []io.Closer
}

type imagerOptions struct {
	intensDev  string
	intensBaud int
	intensAddr int
	haveEcho   bool
	svcAddr    string
	redisAddr  string
}

// Options for the Imager
type Option struct {
	f func(*imagerOptions)
}

// Set the Intensifier serial device
func IntensifierDevice(name string) Option {
	return Option{func(opts *imagerOptions) {
		opts.intensDev = name
	}}
}

// Set the Intensifier baud rate
func IntensifierBaud(baud int) Option {
	return Option{func(opts *imagerOptions) {
		opts.intensBaud = baud
	}}
}

// Set the address of the image acquisition server
func ImageAcqServer(addr string) Option {
	return Option{func(opts *imagerOptions) {
		opts.svcAddr = addr
	}}
}

// Set the address of the Redis server providing the messaging
// interface for PDU commands
func RedisServer(addr string) Option {
	return Option{func(opts *imagerOptions) {
		opts.redisAddr = addr
	}}
}

// New creates a new Imager and opens the communication links to the camera
// and image intensifier.
func New(options ...Option) (*Imager, error) {
	opts := imagerOptions{
		intensDev:  "/dev/ttyS1",
		intensBaud: 115200,
		intensAddr: 7,
		haveEcho:   true,
		svcAddr:    "localhost:10130",
		redisAddr:  "localhost:6379",
	}

	for _, opt := range options {
		opt.f(&opts)
	}

	im := &Imager{}
	im.closers = make([]io.Closer, 0)

	conn, err := grpcClient(opts.svcAddr)
	if err != nil {
		return nil, fmt.Errorf("Cannot connect to gRPC server at %q: %w",
			opts.svcAddr, err)
	}
	im.iClient = imagesvc.NewClient(conn)
	im.closers = append(im.closers, conn)

	im.mtr, err = focus.New(opts.redisAddr)
	if err != nil {
		return nil, err
	}

	p, err := port.SerialPort(opts.intensDev, opts.intensBaud, time.Second*5)
	if err != nil {
		return nil, fmt.Errorf("Open %q failed: %w", opts.intensDev, err)
	}
	im.intensifier = iccd.NewIntensifier(iccd.NewBus(p, opts.haveEcho), opts.intensAddr)
	im.closers = append(im.closers, p)

	return im, nil
}

func (im *Imager) Close() error {
	if im.closers == nil {
		return errors.New("Ports already closed")
	}

	for _, c := range im.closers {
		c.Close()
	}
	im.closers = nil
	return nil
}

// Configure sets the parameters for the camera, image intensifier, and
// focus motor.
func (im *Imager) Configure(cfg Config) error {
	// Configure the image acquisition
	exp := time.Duration(cfg.Iccd.Exposure)
	opts := []imagesvc.AcqOption{
		imagesvc.Trigger(imagesvc.HardwareTrigger),
		imagesvc.Exposure(exp),
		imagesvc.Gain(cfg.Iccd.Gain),
		imagesvc.Timeout(exp + time.Second*5),
	}

	switch {
	case cfg.Focus.Change != "":
		fs, err := NewStepper(cfg.Focus)
		if err != nil {
			return err
		}
		im.fstep = fs
	case cfg.Focus.Start > 0:
		cfg.Focus.Change = "+ 0"
		cfg.Focus.EndState = fmt.Sprintf("== %d", cfg.Focus.Start)
		fs, err := NewStepper(cfg.Focus)
		if err != nil {
			return err
		}
		im.fstep = fs
	default:
		im.fstep = nil
	}

	err := im.iClient.StartAcq(context.Background(), opts...)
	if err != nil {
		return fmt.Errorf("Set camera parameters: %w", err)
	}

	// Configure the image intensifier gate and trigger source.
	err = im.intensifier.SetGate(cfg.Iccd.Gate)
	if err != nil {
		return fmt.Errorf("Set intensifier gate: %w", err)
	}
	err = im.intensifier.SetSync(cfg.Iccd.Sync)
	if err != nil {
		return fmt.Errorf("Set intensifier trigger: %w", err)
	}
	// Set the intensifier gain
	err = im.intensifier.SetGain(cfg.Iccd.Igain)
	if err != nil {
		return fmt.Errorf("Set intensifier gain: %w", err)
	}

	return nil
}

func genFilename(t time.Time, _ int) string {
	s := t.Format("20060102_150405")
	return fmt.Sprintf("img_%s_%03d.tiff", s, t.Nanosecond()/1000000)
}

type AcqOptions struct {
	// Number of images to acquire
	Count int
	// Image storage directory
	Dir string
	// Function to generate an image filename. The input parameters
	// are a timestamp and the image index number; 0 - Count-1
	Filename func(time.Time, int) string
}

type AcqResult struct {
	// Image filename
	Name string
	// Focus motor setting
	Focus int
}

// Acquire reads and stores one or more images
func (im *Imager) Acquire(ctx context.Context, opts AcqOptions) ([]AcqResult, error) {
	ctx = metadata.AppendToOutgoingContext(ctx, "image-chunksize",
		fmt.Sprintf("%d", 64*1024))
	results := make([]AcqResult, 0, opts.Count)
	var (
		name string
		step int
		done bool
	)

	for i := 0; i < opts.Count; i++ {
		if im.fstep != nil {
			// Adjust focus to next step
			step, done = im.fstep.Step()
			if done {
				im.fstep = nil
			} else {
				im.mtr.Goto(ctx, int32(step))
			}
		}

		rdr, err := im.iClient.Acquire(ctx)
		if err != nil {
			return results, fmt.Errorf("Cannot acquire image %d: %w", i, err)
		}

		if opts.Filename == nil {
			name = genFilename(time.Now(), i)
		} else {
			name = opts.Filename(time.Now(), i)
		}

		file, err := os.Create(filepath.Join(opts.Dir, name))
		if err != nil {
			return results, fmt.Errorf("Create file %s: %w", name, err)
		}
		_, err = io.Copy(file, rdr)
		if err != nil {
			return results, fmt.Errorf("Writing file %s: %w", name, err)
		}
		results = append(results,
			AcqResult{Name: file.Name(), Focus: int(im.mtr.Status().Counts)})
		file.Close()

	}
	return results, nil
}

func (im *Imager) Shutdown() error {
	return im.iClient.StopAcq(context.Background())
}
