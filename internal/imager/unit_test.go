package imager

import (
	"reflect"
	"testing"
)

func testSteps(t *testing.T, cfg FocusConfig, expected []int) {
	s, err := NewStepper(cfg)
	if err != nil {
		t.Fatal(err)
	}

	steps := make([]int, 0)
	var (
		done bool
		step int
	)

	for !done {
		step, done = s.Step()
		steps = append(steps, step)
	}

	if !reflect.DeepEqual(steps, expected) {
		t.Errorf("Expected %v, got %v", expected, steps)
	}
}

func TestSteps(t *testing.T) {
	tests := map[string]struct {
		in  FocusConfig
		out []int
	}{
		"add-1": {in: FocusConfig{Start: 0, Change: "+ 10", EndState: "> 30"},
			out: []int{0, 10, 20, 30, 40}},
		"add-2": {in: FocusConfig{Start: 1, Change: "+ 10", EndState: "> 30"},
			out: []int{1, 11, 21, 31}},
		"mult-1": {in: FocusConfig{Start: 10, Change: "* 1.5", EndState: "> 100"},
			out: []int{10, 15, 22, 33, 49, 73, 109}},
		"single": {in: FocusConfig{Start: 10, Change: "+ 0", EndState: "== 10"},
			out: []int{10}},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			testSteps(t, tc.in, tc.out)
		})
	}
}
