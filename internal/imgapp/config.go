package imgapp

import (
	"fmt"
	"io/ioutil"

	"bitbucket.org/uwaploe/invaderacq/internal/imager"
	"bitbucket.org/uwaploe/tbctl/pkg/tb"
	"github.com/BurntSushi/toml"
)

type sysConfig struct {
	Timing tb.Timing          `toml:"timing"`
	Iccd   imager.IccdConfig  `toml:"iccd"`
	Focus  imager.FocusConfig `toml:"focus"`
}

func loadConfig(filename string) (sysConfig, error) {
	var cfg sysConfig

	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return cfg, fmt.Errorf("read parameter file: %w", err)
	}

	_, err = toml.Decode(string(contents), &cfg)

	return cfg, err
}
