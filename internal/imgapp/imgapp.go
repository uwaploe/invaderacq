package imgapp

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"bitbucket.org/uwaploe/invaderacq/internal/imager"
	"bitbucket.org/uwaploe/tbctl/pkg/tb"
)

const Usage = `Usage: imageacq [options] paramfile [count]

Run an inVADER data acquisition sequence and collect COUNT images.
`

type listVal []string

func (l *listVal) String() string {
	return fmt.Sprint(*l)
}

func (l *listVal) Set(value string) error {
	for _, tag := range strings.Split(value, ",") {
		*l = append(*l, tag)
	}
	return nil
}

type appEnv struct {
	fl        *flag.FlagSet
	vers      string
	paramFile string
	imgCount  int
	imOpts    []imager.Option
	tbAddr    string
	outDir    string
	mdFile    string
	tags      listVal
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("invader", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	app.imOpts = make([]imager.Option, 0)

	showVers := fl.Bool("version", false, "Show program version information and exit")
	imSvc := fl.String("server", "", "host:port for image acquisition server")
	intensDev := fl.String("intens-dev", "", "image intensifier serial device")
	fl.StringVar(&app.tbAddr, "tb-sock", "/run/tb.sock", "Timing Board server socket")
	fl.StringVar(&app.outDir, "image-dir",
		lookupEnvOrString("INV_IMAGE_DIR", "."), "image storage directory")
	fl.StringVar(&app.mdFile, "meta",
		lookupEnvOrString("INV_IMAGE_META", ""), "metadata file")
	fl.Var(&app.tags, "tag", "comma separated list of tags to include with the metadata")

	app.fl = fl
	if err := fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) == 0 {
		fmt.Fprintf(os.Stderr, "Missing parameter file\n")
		fl.Usage()
		return flag.ErrHelp
	}
	app.paramFile = req[0]

	if len(req) > 1 {
		app.imgCount, _ = strconv.Atoi(req[1])
	}

	if app.imgCount == 0 {
		app.imgCount = 1
	}

	if *imSvc != "" {
		app.imOpts = append(app.imOpts, imager.ImageAcqServer(*imSvc))
	}

	if *intensDev != "" {
		app.imOpts = append(app.imOpts, imager.IntensifierDevice(*intensDev))
	}

	return nil
}

type metaData struct {
	Image    string    `json:"image"`
	Tags     []string  `json:"tags,omitempty"`
	Settings sysConfig `json:"settings"`
}

func appendMetadata(filename string, cfg sysConfig, res []imager.AcqResult, tags listVal) error {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	enc := json.NewEncoder(f)
	for _, obj := range res {
		cfg.Focus.Setting = obj.Focus
		err = enc.Encode(metaData{Image: filepath.Base(obj.Name), Tags: tags, Settings: cfg})
		if err != nil {
			return err
		}
	}

	return nil
}

func (app *appEnv) run(ctx0 context.Context) error {
	// Load the parameter file
	cfg, err := loadConfig(app.paramFile)
	if err != nil {
		return err
	}

	// Validate the timing board values
	if err := cfg.Timing.Validate(tb.FpgaClock); err != nil {
		return err
	}

	// Load the timing board parameters
	FpgaLoad.Data, _ = json.Marshal(cfg.Timing)
	err = sendMessages(app.tbAddr, FpgaDisable, FpgaLoad)
	if err != nil {
		return fmt.Errorf("Cannot send Timing Board message: %w", err)
	}

	im, err := imager.New(app.imOpts...)
	if err != nil {
		return err
	}

	log.Println("Configuring ICCD")
	err = im.Configure(imager.Config{Iccd: cfg.Iccd, Focus: cfg.Focus})
	if err != nil {
		return err
	}

	// Enable FPGA trigger output
	log.Println("Enabling trigger")
	err = sendMessages(app.tbAddr, FpgaEnable)
	if err != nil {
		return err
	}
	defer sendMessages(app.tbAddr, FpgaDisable)
	defer im.Shutdown()

	log.Println("Acquiring images")
	result, err := im.Acquire(ctx0,
		imager.AcqOptions{Count: app.imgCount, Dir: app.outDir})
	if app.mdFile != "" {
		appendMetadata(app.mdFile, cfg, result, app.tags)
	}

	log.Println("Done")
	return err
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	app.vers = vers
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}
	if err = app.run(ctx); err != nil {
		fmt.Fprintf(os.Stderr, "Runtime error: %v\n", err)
		return 1
	}
	return 0
}
