package imgapp

import (
	"encoding/json"
	"fmt"
	"net"

	"bitbucket.org/uwaploe/tbctl/pkg/tb"
)

// Messages for the Timing Board server
var FpgaEnable tb.Message = tb.Message{Cmd: "enable"}
var FpgaDisable tb.Message = tb.Message{Cmd: "disable"}
var FpgaLoad tb.Message = tb.Message{Cmd: "load"}

func sendMessages(sock string, msgs ...tb.Message) error {
	conn, err := net.Dial("unix", sock)
	if err != nil {
		return fmt.Errorf("Server connect failed: %v", err)
	}
	defer conn.Close()

	enc := json.NewEncoder(conn)
	for _, msg := range msgs {
		err := enc.Encode(msg)
		if err != nil {
			return err
		}
	}
	return nil
}
