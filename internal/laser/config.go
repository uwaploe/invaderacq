package laser

import (
	"bitbucket.org/uwaploe/tbctl/pkg/tb"
)

// Beam expander configuration (deprecated)
type ExpConfig struct {
	Setting int `json:"setting" toml:"setting"`
}

// Viron laser configuration
type VironConfig struct {
	Trigger string      `json:"trigger" toml:"trigger"`
	Warmup  tb.Duration `json:"warmup" toml:"warmup"`
}

type Config struct {
	Exp   ExpConfig   `json:"exp" toml:"exp"`
	Viron VironConfig `json:"viron" toml:"viron"`
}
