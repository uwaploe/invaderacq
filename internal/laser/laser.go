package laser

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"strings"
	"time"

	"bitbucket.org/uwaploe/invaderacq/internal/pducmd"
	"bitbucket.org/uwaploe/invaderacq/internal/port"
	"bitbucket.org/uwaploe/viron"
)

func vironSetup(dev *viron.Device, trigsrc string) error {
	var err error
	switch strings.ToLower(trigsrc) {
	case "external":
		_, err = dev.Exec(viron.NewCommandLine("TRIG", "EE"))
	case "internal":
		_, err = dev.Exec(viron.NewCommandLine("TRIG", "II"))
	default:
		err = fmt.Errorf("Invalid trigger source: %q", trigsrc)
	}
	return err
}

func vironStop(dev *viron.Device) error {
	_, err := dev.Exec(viron.NewCommandLine("STOP"))
	return err
}

func vironStandby(dev *viron.Device) error {
	_, err := dev.Exec(viron.NewCommandLine("STANDBY"))
	return err
}

func vironFire(dev *viron.Device) error {
	_, err := dev.Exec(viron.NewCommandLine("FIRE"))
	return err
}

// Laser represents the inVADER laser subsystem
type Laser struct {
	laserConn net.Conn
	laserDev  *viron.Device
	cmdr      *pducmd.Pducmd
	closers   []io.Closer
	done      chan struct{}
}

type laserOptions struct {
	laserAddr  string
	laserPword string
	redisAddr  string
}

// Set an options for the Laser
type Option struct {
	f func(*laserOptions)
}

// Set the address of the Viron laser network interface
func VironServer(addr string) Option {
	return Option{func(opts *laserOptions) {
		opts.laserAddr = addr
	}}
}

// Set the password for the Viron network interface
func VironPassword(pword string) Option {
	return Option{func(opts *laserOptions) {
		opts.laserPword = pword
	}}
}

// Set the address of the Redis server providing the messaging
// interface for PDU commands
func RedisServer(addr string) Option {
	return Option{func(opts *laserOptions) {
		opts.redisAddr = addr
	}}
}

// New creates a new Laser and opens the communication links to all
// peripheral devices.
func New(options ...Option) (*Laser, error) {
	opts := laserOptions{
		laserAddr: "10.40.7.11:23",
		redisAddr: "localhost:6379",
	}

	for _, opt := range options {
		opt.f(&opts)
	}

	l := &Laser{cmdr: pducmd.New(opts.redisAddr)}
	l.closers = make([]io.Closer, 0)

	p1, err := port.NetworkPort(opts.laserAddr, time.Second*5)
	if err != nil {
		return nil, fmt.Errorf("Cannot access Viron laser at %q: %w", opts.laserAddr, err)
	}
	l.closers = append(l.closers, p1)

	l.laserDev = viron.NewDevice(p1)
	if err = l.laserDev.StartSession(opts.laserPword); err != nil {
		return nil, fmt.Errorf("Cannot log-in to Viron laser: %w", err)
	}

	l.done = make(chan struct{}, 1)
	go l.watchdog(time.Second * 30)

	return l, nil
}

// Service the PDU laser watchdog
func (l *Laser) watchdog(interval time.Duration) {
	ticker := time.NewTicker(interval)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			l.cmdr.Send("LASER,1")
		case <-l.done:
			return
		}
	}
}

func (l *Laser) Close() error {
	err := vironStop(l.laserDev)
	close(l.done)
	if l.closers == nil {
		return errors.New("Ports already closed")
	}

	for _, c := range l.closers {
		c.Close()
	}
	l.closers = nil
	return err
}

func (l *Laser) Configure(cfg Config) error {
	// Setup the Viron laser
	err := vironSetup(l.laserDev, cfg.Viron.Trigger)
	if err != nil {
		return fmt.Errorf("Viron setup failed: %w", err)
	}

	return nil
}

func (l *Laser) Prepare(ctx context.Context, statCh chan<- viron.Info,
	warnOk bool) error {
	err := vironStandby(l.laserDev)
	if err != nil {
		return fmt.Errorf("Standby mode failed: %w", err)
	}

	if warnOk {
		err = l.laserDev.WaitForNoFault(ctx, statCh)
	} else {
		err = l.laserDev.WaitForReady(ctx, statCh)
	}
	if err != nil {
		return fmt.Errorf("Laser not ready: %w", err)
	}

	return vironFire(l.laserDev)
}

func (l *Laser) Shutdown() error {
	return vironStandby(l.laserDev)
}
