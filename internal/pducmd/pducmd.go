// Package pducmd sends commands to the PDU via the pdumon service which
// monitors a Redis pub/sub channel for commands.
package pducmd

import (
	"errors"
	"time"

	"github.com/gomodule/redigo/redis"
)

// Command channel
const PduCommand = "pdu.command"

var ErrNoListeners = errors.New("No listeners")

func newPool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", server)
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
}

type Pducmd struct {
	pool *redis.Pool
}

// New returns a new Pducmd which connects to the Redis server at addr
func New(addr string) *Pducmd {
	return &Pducmd{pool: newPool(addr)}
}

// Send sends a message and returns any error that occurs.
func (c *Pducmd) Send(cmd string) error {
	conn := c.pool.Get()
	defer conn.Close()

	n, err := redis.Int(conn.Do("PUBLISH", PduCommand, []byte(cmd)))
	if err != nil {
		return err
	}

	if n == 0 {
		return ErrNoListeners
	}

	return nil
}
