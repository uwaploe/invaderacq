// Package power manages the power switching for inVADER. The actually
// switching is done by the PDU board which is managed by the pdumon
// service. Communication with pdumon is via Redis pub-sub channels.
package power

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/uwaploe/invaderacq/internal/pducmd"
	"bitbucket.org/uwaploe/pducom"
	"github.com/gomodule/redigo/redis"
)

var ErrInvalidSubsys = errors.New("Invalid subsystem")

type Monitor struct {
	cmdr   *pducmd.Pducmd
	status pducom.Power
	psc    redis.PubSubConn
	cond   *sync.Cond
	done   chan struct{}
}

type rawMessage struct {
	T    time.Time    `json:"time"`
	Src  string       `json:"source"`
	Data pducom.Power `json:"data"`
}

// New returns a new Monitor. Addr is the host:port of the Redis server which
// will provide the control interface.
func New(addr string) (*Monitor, error) {
	conn, err := redis.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}
	psc := redis.PubSubConn{Conn: conn}
	err = psc.Subscribe("data.pdu.POWER")
	if err != nil {
		return nil, err
	}
	m := &Monitor{
		cmdr: pducmd.New(addr),
		done: make(chan struct{}, 1),
		cond: sync.NewCond(&sync.Mutex{}),
		psc:  psc,
	}
	go m.listen(psc)

	// Send a periodic PING to ensure that the listen goroutine doesn't
	// get "stuck".
	ticker := time.NewTicker(time.Second * 10)
	go func() {
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				if err := psc.Ping(""); err != nil {
					psc.Unsubscribe()
				}
			case <-m.done:
				return
			}
		}
	}()

	return m, nil
}

func (m *Monitor) Close() {
	m.psc.Unsubscribe()
}

func allOn(pwr pducom.Power, subsys ...string) bool {
	for _, sys := range subsys {
		if !pwr[sys] {
			return false
		}
	}
	return true
}

func allOff(pwr pducom.Power, subsys ...string) bool {
	for _, sys := range subsys {
		if pwr[sys] {
			return false
		}
	}
	return true
}

// WaitForOff waits for all of the listed subsystems to be powered off or
// for the Context to be canceled.
func (m *Monitor) WaitForOff(ctx context.Context, subsys ...string) error {
	m.cond.L.Lock()
	defer m.cond.L.Unlock()
	for !allOff(m.status, subsys...) {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-m.done:
			return errors.New("Listener stopped")
		default:
			m.cond.Wait()
		}
	}
	return nil
}

// WaitForOn waits for all of the listed subsystems to be powered on or for
// the Context to be canceled.
func (m *Monitor) WaitForOn(ctx context.Context, subsys ...string) error {
	m.cond.L.Lock()
	defer m.cond.L.Unlock()
	for !allOn(m.status, subsys...) {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-m.done:
			return errors.New("Listener stopped")
		default:
			m.cond.Wait()
		}
	}
	return nil
}

// WaitFor Status waits for the power status from the PDU to be updated.
func (m *Monitor) WaitForStatus(ctx context.Context) (pducom.Power, error) {
	m.cond.L.Lock()
	defer m.cond.L.Unlock()
	for len(m.status) == 0 {
		select {
		case <-ctx.Done():
			return m.status, ctx.Err()
		case <-m.done:
			return m.status, errors.New("Listener stopped")
		default:
			m.cond.Wait()
		}
	}
	return m.status, nil
}

// On switches on the power to the listed subsystems
func (m *Monitor) On(subsys ...string) error {
	var err error
	const powerOnDelay = time.Millisecond * 50

	for _, sys := range subsys {
		switch sys {
		case "LASER5":
			continue
		case "LASER12", "LASER28":
			// Enable "laser mode" before powering-on either laser
			m.cmdr.Send("LASER,1")
			err = m.cmdr.Send(fmt.Sprintf("LPOWER,%s,1", sys[5:]))
		default:
			err = m.cmdr.Send(fmt.Sprintf("PWREN,%s,1", sys))
		}
		if err != nil {
			return err
		}
		// Small delay to minimize the in-rush current
		time.Sleep(powerOnDelay)
	}

	return nil
}

// Off switches off the power to the listed subsystems
func (m *Monitor) Off(subsys ...string) error {
	var err error

	for _, sys := range subsys {
		switch sys {
		case "LASER5":
			continue
		case "LASER12", "LASER28":
			err = m.cmdr.Send(fmt.Sprintf("LPOWER,%s,0", sys))
		default:
			err = m.cmdr.Send(fmt.Sprintf("PWREN,%s,0", sys))
		}
		if err != nil {
			return err
		}
	}

	return nil
}

// Monitor the Redis pub-sub channel data.pdu.POWER and update the
// Monitor.status field.
func (m *Monitor) listen(psc redis.PubSubConn) {
	defer close(m.done)
	for {
		switch msg := psc.Receive().(type) {
		case error:
			return
		case redis.Subscription:
			if msg.Count == 0 {
				return
			}
		case redis.Pong:
			m.cond.Broadcast()
		case redis.Message:
			rec := rawMessage{}
			if err := json.Unmarshal(msg.Data, &rec); err != nil {
				continue
			}
			m.cond.L.Lock()
			m.status = rec.Data
			m.cond.Broadcast()
			m.cond.L.Unlock()
		}
	}
}
