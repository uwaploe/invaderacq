// Warm-up the Viron laser and leave it in stand-by mode
package vironprep

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/uwaploe/invaderacq/internal/laser"
	"bitbucket.org/uwaploe/viron"
	"github.com/briandowns/spinner"
)

const Usage = `Usage: vironprep [options]

Warm-up the Viron laser and leave it in stand-by mode.

`

type appEnv struct {
	fl          *flag.FlagSet
	vers        string
	laserOpts   []laser.Option
	laserWarmup time.Duration
	statFile    string
	warnOk      bool
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("invader", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	app.laserOpts = make([]laser.Option, 0)

	showVers := fl.Bool("version", false, "Show program version information and exit")
	fl.DurationVar(&app.laserWarmup, "warmup", time.Minute*3,
		"Viron laser warm-up time limit")
	laserAddr := fl.String("laser-addr",
		lookupEnvOrString("INV_VIRON_ADDR", ""), "Viron laser host:port")
	laserPword := fl.String("laser-pword",
		lookupEnvOrString("INV_VIRON_PWORD", ""), "Viron laser password")
	fl.StringVar(&app.statFile, "laser-stat", "", "file to log laser warmup status")
	fl.BoolVar(&app.warnOk, "warn-ok", false, "ignore laser warnings during warmup")

	app.fl = fl
	if err := fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	if *laserAddr != "" {
		app.laserOpts = append(app.laserOpts, laser.VironServer(*laserAddr))
	}

	if *laserPword != "" {
		app.laserOpts = append(app.laserOpts, laser.VironPassword(*laserPword))
	}

	return nil
}

type infoRec struct {
	T    time.Time  `json:"time"`
	Info viron.Info `json:"info"`
}

func (app *appEnv) run(ctx0 context.Context) (err error) {
	l, err := laser.New(app.laserOpts...)
	if err != nil {
		return err
	}

	log.Println("Configuring laser")
	err = l.Configure(laser.Config{Viron: laser.VironConfig{Trigger: "external"}})
	if err != nil {
		return err
	}

	var info_ch chan viron.Info
	done := make(chan struct{}, 1)

	// Log the laser warm-up status to a file
	if app.statFile != "" {
		info_ch = make(chan viron.Info, 1)
		f, err := os.OpenFile(app.statFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		// Goroutine to read from info_ch and write the file
		go func() {
			defer f.Close()
			enc := json.NewEncoder(f)
			for {
				select {
				case <-done:
					return
				case val := <-info_ch:
					enc.Encode(infoRec{T: time.Now(), Info: val})
				}
			}
		}()
	}

	ctx, cancel := context.WithTimeout(ctx0, app.laserWarmup)

	// Eye candy while the laser is warming up
	s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
	s.Prefix = "Waiting for laser warm-up ... "
	s.FinalMSG = "\n"

	// Run the Prepare method in a goroutine while the spinner
	// is shown
	laser_err := make(chan error, 1)
	go func() { laser_err <- l.Prepare(ctx, info_ch, app.warnOk) }()

	s.Start()
	err = <-laser_err
	cancel()
	// Signal the status-file writer to stop
	close(done)
	s.Stop()

	if err != nil {
		return err
	}

	log.Println("Done")

	return l.Shutdown()
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	app.vers = vers
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}
	if err = app.run(ctx); err != nil {
		fmt.Fprintf(os.Stderr, "Runtime error: %v\n", err)
		return 1
	}
	return 0
}
