package webapp

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
)

const Usage = `Usage: invaderacq [options] [addr]

HTTP server to provide the backend for the inVADER UI.
`

type appEnv struct {
	fl      *flag.FlagSet
	vers    string
	svcAddr string
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("invader", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	app.fl = fl
	if err := fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) > 0 {
		app.svcAddr = req[0]
	}

	return nil
}

func (app *appEnv) run(ctx context.Context) error {

	listeners, err := activation.Listeners()
	if err != nil {
		return fmt.Errorf("Systemd activation error: %w", err)
	}

	var listener net.Listener
	switch n := len(listeners); n {
	case 0:
		if app.svcAddr == "" {
			return errors.New("Server address must be specified")
		}
		// Not activated by a Systemd socket ...
		addr, _ := net.ResolveTCPAddr("tcp", app.svcAddr)
		listener, err = net.ListenTCP("tcp", addr)
		if err != nil {
			return err
		}
	case 1:
		listener = listeners[0]
	default:
		return fmt.Errorf("Unexpected number of socket activation fds: %d", n)
	}

	router := mux.NewRouter()

	srv := &http.Server{
		Handler:      LogRequest(router, os.Stdout),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	idleConnsClosed := make(chan struct{})
	go func() {
		<-ctx.Done()

		// Context cancelled, shutdown the server
		if err := srv.Shutdown(context.Background()); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed)
	}()

	log.Printf("inVADER UI backend starting: %s", app.vers)
	if err := srv.Serve(listener); err != http.ErrServerClosed {
		// Error starting or closing listener:
		return err
	}

	<-idleConnsClosed

	return nil
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	app.vers = vers
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}
	if err = app.run(ctx); err != nil {
		fmt.Fprintf(os.Stderr, "Runtime error: %v\n", err)
		return 1
	}
	return 0
}
